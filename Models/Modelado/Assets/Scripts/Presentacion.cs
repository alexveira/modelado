﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Presentacion : MonoBehaviour {
	public GameObject[] objetos;
	public int number = 0;
	int limite;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonUp ("Fire1") || Input.GetButtonUp ("AltFire1") || (Input.GetAxis("Mouse ScrollWheel") < 0f)) {
			if (number < objetos.Length-1) 
				number++;
		}
		if (Input.GetButtonUp ("Fire2") || Input.GetButtonUp ("AltFire2") || (Input.GetAxis("Mouse ScrollWheel") > 0f)) {
			if (number>0)
			number--;
			
		} 
		objetos [number].SetActive (true);
		for (int i = 0; i < objetos.Length; i++) {
			if(number != i){
				objetos [i].SetActive (false);
			}
		}
	}


}
