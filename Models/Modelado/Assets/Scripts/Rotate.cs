﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {
	public float speed;
	public bool rotatex;
	public bool rotatey;
	public bool rotatez;
	public bool invert;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (invert) {
			if(rotatey){
				gameObject.transform.Rotate (0,-speed*Time.deltaTime,0);
			}
			if(rotatex){
				gameObject.transform.Rotate (-speed*Time.deltaTime,0,0);
			}
			if(rotatez){
				gameObject.transform.Rotate (0,0,-speed*Time.deltaTime);
			}
		}
		if (!invert) {

			if(rotatey){
				gameObject.transform.Rotate (0,speed*Time.deltaTime,0);
			}
			if(rotatex){
				gameObject.transform.Rotate (speed*Time.deltaTime,0,0);
			}
			if(rotatez){
				gameObject.transform.Rotate (0,0,speed*Time.deltaTime);
			}
		}
	}
}
